# TP 3 : On va router des trucs

- [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
- [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
- [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP](#1-mise-en-place-du-serveur-dhcp)
    - [2. Analyse de trames](#2-analyse-de-trames-2)

## I. ARP

### 1. Echange ARP

**🌞 Générer des requêtes ARP**

- Ping de 10.3.1.12 (john) vers 10.3.1.11 (marcel) :
    ```
        [root@john ~]# ping 10.3.1.11
        PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
        64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.343 ms
        64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.520 ms
        64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.646 ms
        ^C
        --- 10.3.1.11 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2030ms
        rtt min/avg/max/mdev = 0.343/0.503/0.646/0.124 ms
    ```
- Ping de 10.3.1.11 (marcel) vers 10.3.1.12 (john) :
    ```
        [root@marcel ~]# ping 10.3.1.12
        PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
        64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.936 ms
        64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.701 ms
        64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.450 ms
        ^C
        --- 10.3.1.12 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2047ms
        rtt min/avg/max/mdev = 0.450/0.695/0.936/0.198 ms
    ```

- Observation des tables ARP des deux machines & récupération des adresses MAC :
    ```
        # Table ARP de marcel avec MAC de john parmis elles :
        [root@marcel ~]# ip neigh show
        10.3.1.12 dev enp0s3 lladdr 08:00:27:a0:74:35 REACHABLE

        # Table ARP de john avec MAC de marcel parmis elles :
        [root@john ~]# ip neigh sh
        10.3.1.11 dev enp0s3 lladdr 08:00:27:81:e8:9c REACHABLE
    ```

    ```
        # Affichage de la mac de marcel depuis sa machine. Celle dans la table ARP de jhon est donc correcte :
        [root@marcel ~]# ip link show

        2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
        link/ether 08:00:27:81:e8:9c brd ff:ff:ff:ff:ff:ff
    ```

---

### 2. Analyse de trames

**🌞 Analyse de trames**

🦈 [Capture réseau `tp2_arp.pcapng` qui contient un ARP request et un ARP reply](./sources/tp2_arp.pcapng)

## II. Routage

### 1. Mise en place du routage

**🌞 Activer le routage sur le noeud `router`**

```
    root@router# sudo firewall-cmd --list-all
    root@router# sudo firewall-cmd --get-active-zone


    root@router# sudo firewall-cmd --add-masquerade --zone=public
    root@router# sudo firewall-cmd --add-masquerade --zone=public --permanent
```

**🌞 Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

```
    # Pour john
    ip route add default 10.3.1.254 via 10.3.1.254 dev enp0s8

    # Pour marcel
    ip route add default 10.3.2.254 via 10.3.2.254 dev enp0s8
```

Verifions que les deux machines peuvent se joindre :

```
    [root@john ~]# ping 10.3.2.12
    PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
    64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=0.782 ms
    64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=0.661 ms
    64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=0.781 ms
    ^C
    --- 10.3.2.12 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2029ms
    rtt min/avg/max/mdev = 0.661/0.741/0.782/0.056 ms
```

---

### 2. Analyse de trames

**🌞 Analyse des échanges ARP**

| ordre | type trame  | IP source  | MAC source                 | IP destination | MAC destination            |
|-------|-------------|------------|----------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.3.1.11  | `john` `08:00:27:a0:74:35` | 10.3.1.254     | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.3.1.254 | Broadcast `FF:FF:FF:FF:FF` | 10.3.1.11      | `john` `08:00:27:a0:74:35` |


🦈 [Capture réseau `tp2_routage_marcel.pcap`](./sources/tp2_routage_marcel.pcap)

---

### 3. Accès internet

**🌞 Donnez un accès internet à vos machines**

```
IN PROGRESS
```

**🌞 Analyse de trames**

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |     |
|-------|------------|--------------------|-------------------------|----------------|-----------------|-----|
| 1     | ping       | `john` `10.3.1.12` | `john` `AA:BB:CC:DD:EE` | `8.8.8.8`      | ?               |     |
| 2     | pong       | ...                | ...                     | ...            | ...             | ... |

🦈 [Capture réseau `tp2_routage_internet.pcap`](./sources/tp2_routage_internet.pcap)

## III. DHCP

### 1. Mise en place du serveur DHCP

**🌞 Sur la machine `john`, vous installerez et configurerez un serveur DHCP**

- Configuration du DHCP server :
    ```
        root@john dhcp-server# sudo nano /etc/dhcp/dhcpd.conf

        root@john dhcp-server# cat /etc/dhcp/dhcpd.conf

        default-lease-time 900;
        max-lease-time 10800;

        authoritative;

        subnet 10.3.1.0 netmask 255.255.255.0 {
        range 10.3.1.20 10.3.1.250;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 10.3.1.1;
        }
    ```

**🌞 Améliorer la configuration du DHCP**

    ```
        root@john dhcp-server# cat /etc/dhcp/dhcpd.conf

        default-lease-time 900;
        max-lease-time 10800;

        authoritative;

        subnet 10.3.1.0 netmask 255.255.255.0 {
        range 10.3.1.20 10.3.1.250;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 10.3.1.1;
        }
    ```

    ```
        [root@bob ~] sudo dhclient
        [bob@localhost ~] ip a
        2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether 08:00:27:14:d1:02 brd ff:ff:ff:ff:ff:ff
            inet 10.3.1.53/24 brd 10.3.1.255 scope global dynamic enp0s3
            valid_lft 943sec preferred_lft 943sec
            inet6 fe80::a00:27ff:fe14:d102/64 scope link noprefixroute 
            valid_lft forever preferred_lft forever

        [root@bob ~] ping 10.3.1.254
        PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
        64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.555 ms


        [root@bob ~] ping google.com
        PING google.com (142.250.179.110) 56(84) bytes of data.
        64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=61 time=12.3 ms
    ```

---

### 2. Analyse de trames

**🌞 Analyse de trames**

🦈 [Capture réseau `tp2_dhcp.pcapng`](./sources/tp2_dhcp.pcapng)