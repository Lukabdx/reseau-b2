# TP 1 : Mise en jambes

- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [A. En ligne de commande](#a-en-ligne-de-commande)
    - [B. En graphique (GUI : Graphical User Interface)](#b-en-graphique-gui--graphical-user-interface)
    - [C. Questions](#c-questions)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)
- [Bilan](#bilan)

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

#### A. En ligne de commande

**🌞 Affichez les infos des cartes réseau de votre PC**

- nom, adresse MAC et adresse IP de l'interface WiFi
    ```
    PS C:\Users\lukabdx> ipconfig /all
    [...]
    Carte réseau sans fil Wi-Fi :
        Adresse physique . . . . . . . . . . . : 80-B6-55-EC-E7-17
        [...]
        Adresse IPv6 de liaison locale. . . . .: fe80::ad83:882b:d646:46c5%8(préféré)
        Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.206(préféré)
        Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    [...]
    ```
- nom, adresse MAC et adresse IP de l'interface Ethernet
    ```
    Je n'ai pas de carte Ethernet sur mon PC.
    ```

**🌞 Affichez votre gateway**

```
PS C:\Users\lukabdx> ipconfig
[...]
Carte réseau sans fil Wi-Fi :
    [...]
    Passerelle par défaut. . . . . . . . . : 10.33.19.254
[...]
```

#### B. En graphique (GUI : Graphical User Interface)

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

```
Petit tuto pour Windows :
    1 - Aller dans le "Panneau de configuration"
    2 - Aller dans "Réseau et Internet"
    3 - Aller dans "centre de réseau et de partage"
    4 - Cliquer sur la carte en questions dans la partie "Connexions (ex : Wi-Fi WiFi@YNOV)"
    5 - Aller dans les "Détails"
```
Capture d'écran de l'interface disponible [à ce lien](./sources/screenshot-1.png).

#### C. Questions

**🌞 A quoi sert la gateway dans le réseau d'YNOV ?**

La gateway fait office de passerelle avec les autres réseaux. Cela rend accessible le WAN à partir de notre LAN.
En gros, ça sert à aller sur Internet :)

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)

**🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP**

```
Petit tuto pour Windows (épisode 2) :
    1 - Sur l'onglet de la carte en question (voir tuto épisode 1), cliquer sur "Propriétés" au lieu de "Détails"
    2 - Cliquer sur "Protocole Internet Version 4 (TCP/IPv4)
```
Capture d'écran de l'interface disponible [à ce lien](./sources/screenshot-2.png).

Preuve que le changement a été pris en compte :
```
PS C:\Users\lukabdx> ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :
    Adresse physique . . . . . . . . . . . : 80-B6-55-EC-E7-17
    [...]
    Adresse IPv6 de liaison locale. . . . .: fe80::ad83:882b:d646:46c5%8(préféré)
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.31(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
[...]
```

**🌞 Expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération**

Comme dit dans l'énoncé, on pourra envoyer des paquets, mais pas en recevoir.
En effet, le routeur sera dans l'impossibilité de savoir vers quel appareil il doit poser le envoyer les paquets.
Comme les connexions à internet sont divers échanges de paquets dans les deux sens, si l'on ne peut faire transiter les paquets que dans un sens, la communication est coupée.

## II. Exploration locale en duo

### 3. Modification d'adresse IP

Capture d'écran de l'interface de changement d'IP [à ce lien](./sources/screenshot-3.png) (PC 1).

Capture d'écran de l'interface de changement d'IP [à ce lien](./sources/screenshot-4.png) (PC 2).

```bash
# vérification des changements sur le premier ordinateur
C:\Users\Matteo>ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::e1db:4bc1:6c23:6d5f%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.137.1
```

```bash
# vérification des changements sur le deuxième ordinateur
Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::dd19:ecee:b04e:a3e7%22
   IPv4 Address. . . . . . . . . . . : 192.168.137.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.252
```   

```bash
# ping du PC1 vers le PC2
PS C:\Users\Dreasy> ping 192.168.137.2

Pinging 192.168.137.2 with 32 bytes of data:
Reply from 192.168.137.2: bytes=32 time<1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=2ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128

Ping statistics for 192.168.137.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

```bash
# ping du PC2 vers le PC1
C:\Users\Matteo>ping 192.168.137.1

Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.137.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

```bash
# affichage et consultation de la table ARP du premier ordinateur
Interface: 192.168.137.1 --- 0x16
  Internet Address      Physical Address      Type
  192.168.137.2           04-d4-c4-e6-15-34     dynamic
  192.168.137.3           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
```

```bash
# affichage et consultation de la table ARP du second ordinateur
C:\Users\Matteo>arp -a

[...]

Interface : 192.168.137.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.137.1           a0-ce-c8-38-0c-17     dynamique
  192.168.137.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### 4. Utilisation d'un des deux comme gateway

```bash
# Requête vers un serveur connu
C:\Users\Matteo>ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=24 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=21 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 21ms, Maximum = 24ms, Moyenne = 22ms
```

```bash
# Vérification du passage des paquets par la passerelle
C:\Users\Matteo>tracert 1.1.1.1

Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :

  1    <1 ms     *        2 ms  DREASY [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     5 ms     5 ms     5 ms  10.33.19.254
  4     6 ms     6 ms     6 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
  5    14 ms    12 ms    12 ms  108.97.30.212.rev.sfr.net [212.30.97.108]
```

### 5. Petit chat privé

```bash
# Infos visibles depuis le pc serveur

PS C:\Users\Dreasy> ncat -l -p 8888
Coucou
Miew
g fAIm
```

```bash
# Infos visibles depuis le pc client

PS C:\Users\Matteo> ncat  192.168.137.1 8888
libnsock ssl_init_helper(): OpenSSL legacy provider failed to load.

Coucou
Miew
g fAIm
```

### 6. Firewall

Apres plusieurs essais l'installation du 'bon' netcat n'a pas pu aboutir suite a des interdictions de windows malgrés tout les antivirus/firewall desactiver 

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

```bash
# Affichage de l'adresse IP du DHCP

Carte réseau sans fil Wi-Fi :

[...]
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

```bash
# Le bail DHCP expire le jeudi 29 septembre 2022 à 9h16. Il a une durée de 24 heures qui peut être modifié par un administrateur.
C:\Users\33785>ipconfig /all

Configuration IP de Windows

[...]
   Bail obtenu. . . . . . . . . . . . . . : mercredi 28 septembre 2022 09:16:42
   Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 09:16:42
[...]
```

### 2. DNS

```bash
# Adresse du serveur DNS que connait mon ordinateur
Carte réseau sans fil Wi-Fi :

[...]
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

```bash
# Affichage de l'adresse IP de google.com, on fait la requête au serveur DNS de google, l'adresse IP de google.com est 142.250.75.228. 

C:\Users\33785>nslookup www.google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.google.com
Addresses:  2a00:1450:4007:80d::2004
          142.250.75.228
```

```bash
# Affichage de l'adresse IP d'ynov.com, on fait la requête au serveur DNS de google, l'adresse IP d'ynov.com est 104.26.10.233.

C:\Users\33785>nslookup www.ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          172.67.74.226
          104.26.11.233
          104.26.10.233
```

```bash
L'adresse IP du serveur auprès duquel on fait des requêtes est celle du serveur DNS de google, 8.8.8.8
```

```bash
# Le nom de domaine associé à l'adresse IP est host-78-74-21-21.homerun.telia.com
C:\Users\33785>nslookup 78.74.21.21
Serveur :   dns.google
Address:  8.8.8.8

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```

```bash
# Il n'y a pas de nom de domaine associé à cette adresse IP

C:\Users\33785>nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain
```

## IV. Wireshark

```bash
# Changement d'adresse IP parce que je ne suis pas à Ynov

C:\Users\33785>ipconfig

Configuration IP de Windows

[...]

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6. . . . . . . . . . . . . .: 2a04:cec0:1139:cd55:7938:13dc:dac0:406
   Adresse IPv6 temporaire . . . . . . . .: 2a04:cec0:1139:cd55:c9b0:a197:7de2:6976
   Adresse IPv6 de liaison locale. . . . .: fe80::7938:13dc:dac0:406%20
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.43.114
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::9fc4:fa7a:e4:6fe1%20
                                       192.168.43.1
```

```bash
# Ping vers la passerelle

C:\Users\dreasy>ping 192.168.43.1

Envoi d’une requête 'Ping'  192.168.43.1 avec 32 octets de données :
Réponse de 192.168.43.1 : octets=32 temps=2 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=4 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=8 ms TTL=64

Statistiques Ping pour 192.168.43.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 8ms, Moyenne = 4ms
```

Capture d'écran lors du ping du client vers la passerelle [à ce lien](./sources/screenshot-5.png).


  - un `netcat` entre vous et votre mate, branché en RJ45


Apres plusieurs essais l'installation du 'bon' netcat n'a pas pu aboutir suite a des interdictions de windows malgrés tout les antivirus/firewall desactiver 
Suite a cela cette partie n'a pas pu etre terminer.

## Bilan

**🌞 Ce soleil est un troll.**

![No response](https://http.cat/444)